#!/usr/local/bin/php
<?php
# prep variables
$deck = [];
$suits = ['D', 'H', 'C', 'S'];
$suit_cards =  ['A', '2','3','4','5','6','7','8','9','10', 'J', 'Q', 'K'];

# create the deck
foreach ($suits as $suit) {
    foreach ($suit_cards as $suit_card) {
        $deck[] = $suit.$suit_card;
    }
}

# shuffle the deck
shuffle($deck);

# could use array_rand(), but since we have already shuffled can just slice instead
$random5 = array_slice($deck, 0, 5);

# print the 5 random
print_r($random5);
?>
